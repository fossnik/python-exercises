"""Functions for creating, transforming, and adding prefixes to strings."""
import string


def add_prefix_un(word):
    """Take the given word and add the 'un' prefix.

    :param word: str - containing the root word.
    :return: str - of root word prepended with 'un'.
    """

    return "un" + str(word)


def make_word_groups(vocab_words):
    """Transform a list containing a prefix and words into a string with the prefix followed by the words with prefix prepended.

    :param vocab_words: list - of vocabulary words with prefix in first index.
    :return: str - of prefix followed by vocabulary words with
            prefix applied.

    This function takes a `vocab_words` list and returns a string
    with the prefix and the words with prefix applied, separated
     by ' :: '.

    For example: list('en', 'close', 'joy', 'lighten'),
    produces the following string: 'en :: enclose :: enjoy :: enlighten'.
    """

    # get the prefix from the first list element
    prefix = vocab_words[0]

    # delete the first item (the prefix)
    del vocab_words[0]

    # new list for compiling the words (the prefix is first element)
    new_list = [prefix]

    # add the prefix to all words
    for word in vocab_words:
        new_list.append(prefix + word)

    # join up a new list
    remit = " :: ".join(new_list)

    return remit


def remove_suffix_ness(word):
    """Remove the suffix from the word while keeping spelling in mind.

    :param word: str - of word to remove suffix from.
    :return: str - of word with suffix removed & spelling adjusted.

    For example: "heaviness" becomes "heavy", but "sadness" becomes "sad".
    """

    shortened = str(word).removesuffix("ness")

    # change i to y if necessary
    if shortened.endswith("i"):
        shortened = shortened.removesuffix("i") + "y"

    return shortened


def adjective_to_verb(sentence, index):
    """Change the adjective within the sentence to a verb.

    :param sentence: str - that uses the word in sentence.
    :param index: int - index of the word to remove and transform.
    :return: str - word that changes the extracted adjective to a verb.

    For example, ("It got dark as the sun set", 2) becomes "darken".
    """

    wordlist = str(sentence).split()

    the_word = wordlist[index]

    # remove punctuation
    the_word_nopunc = the_word.translate(str.maketrans("", "", string.punctuation))

    return the_word_nopunc + "en"