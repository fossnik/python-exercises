def square_recur(grains_on_current_square, current_square_n, last_square_n):
    if current_square_n == last_square_n:
        return grains_on_current_square

    return square_recur(grains_on_current_square * 2, current_square_n + 1, last_square_n)

def square(number):
    if number < 1 or number > 64:
        raise ValueError('square must be between 1 and 64')

    grains_on_first_square = 1
    first_square_n = 1
    last_square_n = number

    return square_recur(grains_on_first_square, first_square_n, last_square_n)

def total():
    return square(64) * 2 - 1
