MATCHING_VOWELS = ["a", "e", "i", "o", "u", "xr", "yt"]
CONSONANT_CLUSTERS = ["ch", "sch", "thr", "th", "rh", "qu", "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q",
                      "r", "s", "t", "v", "w", "x", "z"]


# will return matching sound if the string starts with any of the items
def string_starts_with_match(string, list_of_matching):
    for item in list_of_matching:
        if string.startswith(item):
            return item

    return False


def translate(text):
    splits = text.split()
    remit_string = ""
    for t in splits:
        remit_string += translate_one(t) + " "

    return remit_string.rstrip()


def translate_one(text):
    # Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end
    # of the word. Please note that "xr" and "yt" at the beginning of a word
    # make vowel sounds (e.g. "xray" -> "xrayay", "yttria" -> "yttriaay").
    if string_starts_with_match(text, MATCHING_VOWELS):
        return text + "ay"

    if text.startswith("y"):  # rule 4
        # Rule 4: If a word contains a "y" after a consonant cluster or as the second
        # letter in a two letter word it makes a vowel sound
        # (e.g. "rhythm" -> "ythmrhay", "my" -> "ymay").
        return text.removeprefix("y") + "yay"

    # Rule 2: If a word begins with a consonant sound, move it to the end of
    # the word and then add an "ay" sound to the end of the word. Consonant
    # sounds can be made up of multiple consonants, a.k.a. a consonant
    # cluster (e.g. "chair" -> "airchay").
    consonant_cluster = string_starts_with_match(text, CONSONANT_CLUSTERS)
    if consonant_cluster:
        if text.removeprefix(consonant_cluster).startswith("qu"):
            # Rule 3: If a word starts with a consonant sound followed by "qu", move it
            # to the end of the word, and then add an "ay" sound to the end of the word
            # (e.g. "square" -> "aresquay").
            return text.removeprefix(consonant_cluster + "qu") + consonant_cluster + "quay"
        else:  # rule 3
            return f"{text.removeprefix(consonant_cluster)}{consonant_cluster}ay"
