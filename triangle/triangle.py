def is_valid_triangle(sides):
    largest_side = max(sides)
    return largest_side < sum(sides) / 2


def equilateral(sides):
    a = sides[0]
    b = sides[1]
    c = sides[2]

    is_valid = is_valid_triangle(sides)
    meets_definition = a == b == c
    return is_valid and meets_definition


def isosceles(sides):
    a = sides[0]
    b = sides[1]
    c = sides[2]

    is_valid = is_valid_triangle(sides)
    meets_definition = a == b or a == c or b == c
    return is_valid and meets_definition


def scalene(sides):
    a = sides[0]
    b = sides[1]
    c = sides[2]

    is_valid = is_valid_triangle(sides)
    meets_definition = a != b != c != a
    return is_valid and meets_definition
