"""Functions for tracking poker hands and assorted card tasks.

Python list documentation: https://docs.python.org/3/tutorial/datastructures.html
"""


def get_rounds(number):
    """Create a list containing the current and next two round numbers.

    :param number: int - current round number.
    :return: list - current round and the two that follow.
    """

    current_and_next_two_round_nums = [number]

    number += 1
    current_and_next_two_round_nums.append(number)

    number += 1
    current_and_next_two_round_nums.append(number)

    return current_and_next_two_round_nums


def concatenate_rounds(rounds_1, rounds_2):
    """Concatenate two lists of round numbers.

    :param rounds_1: list - first rounds played.
    :param rounds_2: list - second set of rounds played.
    :return: list - all rounds played.
    """

    # concatenate rounds
    for i in rounds_2:
        rounds_1.append(i)

    return rounds_1

def list_contains_round(rounds, number):
    """Check if the list of rounds contains the specified number.

    :param rounds: list - rounds played.
    :param number: int - round number.
    :return: bool - was the round played?
    """

    for i in rounds:
        if i == number:
            return True

    return False


def card_average(hand):
    """Calculate and returns the average card value from the list.

    :param hand: list - cards in hand.
    :return: float - average value of the cards in the hand.
    """

    # keep the sum
    sum = 0

    # accrue the sum
    for i in hand:
        sum += i

    # get the average
    average = sum / len(hand)

    return average


def approx_average_is_average(hand):
    """Return if an average is using (first + last index values ) OR ('middle' card) == calculated average.

    :param hand: list - cards in hand.
    :return: bool - does one of the approximate averages equal the `true average`?
    """

    # get the exact average
    card_sum = sum(hand)
    overall_average = card_sum / len(hand)

    # get high and low cards
    high_card = max(hand)
    low_card = min(hand)

    # get the average of high and low cards
    high_low_average = (high_card + low_card) / 2

    # if the average of the high and low equal the overall average
    if overall_average == high_low_average:
        return True

    # get the middle card
    sorted_hand = sorted(hand)
    middle_index = int((len(sorted_hand) - 1) / 2)
    median_card = sorted_hand[middle_index]

    # if the middle card equals the average of all cards, go nuts
    if median_card == overall_average:
        return True

    # nothing came true so return false
    return False

def average_even_is_average_odd(hand):
    """Return if the (average of even indexed card values) == (average of odd indexed card values).

    :param hand: list - cards in hand.
    :return: bool - are even and odd averages equal?
    """

    odds = []
    evens = []

    for card in hand:
        if card % 2 == 0:
            evens.append(card)
        else:
            odds.append(card)

    # calculate averages
    sum_of_even_cards = sum(evens)
    # avoid divide by zero
    if sum_of_even_cards == 0:
        average_of_even = 0
    else:
        average_of_even = sum_of_even_cards / len(evens)

    sum_of_odd_cards = sum(odds)
    if sum_of_odd_cards == 0:
        average_of_odd = 0
    else:
        average_of_odd = sum_of_odd_cards / len(odds)

    # return the truth
    return average_of_even == average_of_odd


def maybe_double_last(hand):
    """Multiply a Jack card value in the last index position by 2.

    :param hand: list - cards in hand.
    :return: list - hand with Jacks (if present) value doubled.
    """

    last_index = len(hand) - 1

    if hand[last_index] == 11:
        hand[last_index] = 22

    return hand
