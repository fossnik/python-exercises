def convert(number):
    remit_string = ""
    if number % 3 == 0:
        remit_string += "Pling"

    if number % 5 == 0:
        remit_string += "Plang"

    if number % 7 == 0:
        remit_string += "Plong"

    if len(remit_string) == 0:
        return str(number)
    else:
        return remit_string
