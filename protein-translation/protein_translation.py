def proteins(strand):
	protein = []
	index = 0
	while index < len(strand):
		code = strand[index:index + 3]
		index = index + 3
		if {'UAA', 'UAG', 'UGA'}.__contains__(code):
			return protein
		else:
			protein.append(codon(code))
	return protein


def codon(c):
	return {
		'AUG': 'Methionine',
		'UUU': 'Phenylalanine',
		'UUC': 'Phenylalanine',
		'UUA': 'Leucine',
		'UUG': 'Leucine',
		'UCU': 'Serine',
		'UCC': 'Serine',
		'UCA': 'Serine',
		'UCG': 'Serine',
		'UAU': 'Tyrosine',
		'UAC': 'Tyrosine',
		'UGU': 'Cysteine',
		'UGC': 'Cysteine',
		'UGG': 'Tryptophan'
	}[c]
