import math


def private_key(p):
    # return number that is less than p and greater than 1
    return math.random(1, p)


def public_key(p, g, private):
    return (g ** p) % private


def secret(p, public, private):
    pass
