def step_to_it(number, iteration):
    # base case
    if number == 1:
        return iteration

    # number is even
    if number % 2 == 0:
        new_number = number / 2

    # number is odd
    else:
        new_number = (number * 3) + 1

    # iterate
    return step_to_it(new_number, iteration + 1)

def steps(number):
    if number < 1:
        raise ValueError('Only positive integers are allowed')

    return step_to_it(number, 0)
