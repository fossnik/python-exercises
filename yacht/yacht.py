# Score categories.
# Change the values as you see fit.
YACHT = "YACHT"
ONES = 1
TWOS = 2
THREES = 3
FOURS = 4
FIVES = 5
SIXES = 6
FULL_HOUSE = "FULL_HOUSE"
FOUR_OF_A_KIND = "FOUR_OF_A_KIND"
LITTLE_STRAIGHT = "LITTLE_STRAIGHT"
BIG_STRAIGHT = "BIG_STRAIGHT"
CHOICE = "CHOICE"

# get all the unique numbers in a set of dice
# for example, [1,2,2,3] would be [1,2,3]
def get_unique_numbers(dice):
    return set(dice)

# get the number that occurs most in the set of dice
def get_most_occurring_number(dice):
    dice_set = get_unique_numbers(dice)
    most_occurring = dice[0]
    for i in dice_set:
        if dice.count(i) >= most_occurring:
            most_occurring = i

    return most_occurring


def score(dice, category):
    return category(dice)
    # # go for Ones, Twos, Threes, etc
    # if str.isdigit(str(category)):
    #     return dice.count(category) * category
    # elif category == "FULL_HOUSE":
    #     for i in set(dice):
    #         if dice.count(i) > 3 or dice.count(i) < 2:
    #             return 0
    #     return sum(dice)
    # elif category == "LITTLE_STRAIGHT":
    #     if len(set(dice)) == 5:
    #         if not list.__contains__(dice, 6):
    #             return 30
    # elif category == "BIG_STRAIGHT":
    #     if len(set(dice)) == 5:
    #         if not list.__contains__(dice, 1):
    #             return 30
    # elif category == "FOUR_OF_A_KIND":
    #     most_occurring = get_most_occurring_number(dice)
    #     if dice.count(most_occurring) >= 4:
    #         return most_occurring * 4
    # elif category == "YACHT":
    #     if len(set(dice)) == 1:
    #         return 50
    # elif category == "CHOICE":
    #     return sum(dice)
    #
    # return 0